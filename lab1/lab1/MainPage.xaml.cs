﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace lab1
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        void firstColor(object sender, EventArgs e)
        {
            first.BackgroundColor = Color.Blue;
            first.Text = "The background is Blue";
        
        }
           
        void secondColor(object sender, EventArgs e)
        {
            first.BackgroundColor = Color.Red;
            first.Text = "The background is Red";
        }
    }
}
